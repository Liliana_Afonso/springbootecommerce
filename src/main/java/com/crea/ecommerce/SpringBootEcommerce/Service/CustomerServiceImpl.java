package com.crea.ecommerce.SpringBootEcommerce.Service;

import com.crea.ecommerce.SpringBootEcommerce.Model.Customer;
import com.crea.ecommerce.SpringBootEcommerce.Repository.CustomerRepository;
import com.crea.ecommerce.SpringBootEcommerce.WebDto.CustomerRegistrationDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Customer save(CustomerRegistrationDto registrationDto) {
        Customer customer = new Customer(registrationDto.getFirstname(), registrationDto.getLastname(),
                registrationDto.getEmail(), passwordEncoder.encode(registrationDto.getPassword()));
        return customerRepository.save(customer);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Customer customer = customerRepository.findByEmail(username);
        if (customer == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        UserDetails user = User.withUsername(customer.getEmail()).password(customer.getPassword()).authorities("USER")
                .build();
        return user;

    }

}
