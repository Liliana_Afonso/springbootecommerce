package com.crea.ecommerce.SpringBootEcommerce.Service;

import com.crea.ecommerce.SpringBootEcommerce.Model.Customer;
import com.crea.ecommerce.SpringBootEcommerce.WebDto.CustomerRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface CustomerService extends UserDetailsService {
    Customer save(CustomerRegistrationDto registrationDto);
}
