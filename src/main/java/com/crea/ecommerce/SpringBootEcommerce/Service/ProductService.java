package com.crea.ecommerce.SpringBootEcommerce.Service;

import java.util.List;
import com.crea.ecommerce.SpringBootEcommerce.Model.Product;
import com.crea.ecommerce.SpringBootEcommerce.Repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repo;

    public List<Product> listAll() {
        return repo.findAll();
    }

    public void save(Product product) {
        repo.save(product);
    }

    public Product get(int id) {
        return repo.findById(id).get();
    }

    public void delete(int id) {
        repo.deleteById(id);
    }
}
