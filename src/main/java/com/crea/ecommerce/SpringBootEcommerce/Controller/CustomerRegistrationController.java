package com.crea.ecommerce.SpringBootEcommerce.Controller;

import com.crea.ecommerce.SpringBootEcommerce.Service.CustomerService;
import com.crea.ecommerce.SpringBootEcommerce.WebDto.CustomerRegistrationDto;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/registration")
public class CustomerRegistrationController {

    private CustomerService customerService;

    public CustomerRegistrationController(CustomerService customerService) {
        super();
        this.customerService = customerService;
    }

    @ModelAttribute("customer")
    public CustomerRegistrationDto customerRegistrationDto() {
        return new CustomerRegistrationDto();
    }

    @GetMapping
    public String showRegistrationForm() {
        return "Registration";
    }

    @PostMapping
    public String registrationCustomerAccount(@ModelAttribute("customer") CustomerRegistrationDto registrationDto) {
        customerService.save(registrationDto);
        // return "redirect:/registration?success";
        return "redirect:/";
    }

}
