package com.crea.ecommerce.SpringBootEcommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
public class SpringBootEcommerceApplication {

	// START : Open Mamp then run then https://localhost:8443
	public static void main(String[] args) {
		SpringApplication.run(SpringBootEcommerceApplication.class, args);

	}

}
