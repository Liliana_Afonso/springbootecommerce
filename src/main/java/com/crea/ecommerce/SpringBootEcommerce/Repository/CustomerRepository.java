package com.crea.ecommerce.SpringBootEcommerce.Repository;

import com.crea.ecommerce.SpringBootEcommerce.Model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
    Customer findByEmail(String email);

}
