// API CURRENCY RATE
const select = document.querySelectorAll('#currencies > div > select')
const input = document.querySelectorAll('#currencies > div > input')
const API = "https://api.exchangeratesapi.io/latest";
let html = "";

async function currency() {

    const res = await fetch(API);
    console.log(res);

    const data = await res.json();
    console.log(data);

    // Get the currency
    const arrKeys = Object.keys(data.rates);
    console.log(arrKeys);

    // Get the currency and the rate
    const rate = data.rates;
    console.log(rate);

    // Add the currency in the html code 
    arrKeys.map(currency => {
        return html += `<option value=${currency}>${currency}</option>`;
    });

    // Create as many attribut select as there are the numbre of currencies and insert the currency
    for (let i = 0; i < select.length; i++) {
        select[i].innerHTML = html
    }

    // Calculation of the rate convertion
    function convert(i, j) {
        input[i].value = input[j].value * rate[select[i].value] / rate[select[j].value];
    }

    input[0].addEventListener("keyup", () => convert(1, 0))
    input[1].addEventListener("keyup", () => convert(0, 1))
    input[0].addEventListener("keydown", () => convert(1, 0))
    input[1].addEventListener("keydown", () => convert(0, 1))


}

currency();



// API RANDOM PERSONS 
// When the page is refreshed the person change
fetch("https://randomuser.me/api/")
    .then(res => {
        return res.json()
    })
    .then(data => {
        console.log(data.results[0])

        var firstName = data.results[0].name.first;
        var lastName = data.results[0].name.last;
        var profilPicture = data.results[0].picture.thumbnail;

        $(".name").html(firstName + " " + lastName);
        $(".profil-picture").attr('src', profilPicture)
    })
    .catch(error => {
        console.log("ERROR")
        $("#notifications").css("display", "none");
    })



